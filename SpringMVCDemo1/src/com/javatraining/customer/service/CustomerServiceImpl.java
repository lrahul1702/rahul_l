package com.javatraining.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.model.Customer;

public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;
	
	public int insertCustomer(Customer customer) {
		
		return customerDAO.insertCustomer(customer);
	}

	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deleteCustomer(int customerId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Customer findByCustomerId(int customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isCustomerExists(int customerId) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Customer> listAllCustomers() {
		// TODO Auto-generated method stub
		return null;
	}

}
