package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;
import com.javatraining.customer.service.CustomerService;
import com.javatraining.customer.service.CustomerServiceImpl;
import com.model.Guest;

@Controller
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	@RequestMapping("/CustomerInfo")
	public ModelAndView getGuest(Customer customer)
	{
		ModelAndView view= new ModelAndView();
		
		int rows=customerService.insertCustomer(customer);
		view.setViewName("customerDetails");
		view.addObject("customerInfo",(customer.getCustomerName()+",you have updated "+rows+"row successfully."));
		return view;
	}
}
