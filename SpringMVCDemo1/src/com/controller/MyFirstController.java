package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Guest;

@Controller
public class MyFirstController {

	@RequestMapping("/good")
	public String pp()
	{
		return "rahul";
	}
	@RequestMapping("/bad")
	public String pp1()
	{
		return "2";
	}
	@RequestMapping("/okay")
	public String pp2()
	{
		return "3";
	}
	@RequestMapping("/not")
	public String pp3()
	{
		return "4";
	}
	@RequestMapping("/insert")
	public ModelAndView getA()
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("in");
		view.addObject("message", "Good morning From Controller");
		return view;
	}
	@RequestMapping("/guest")
	public ModelAndView getGuest(Guest guest)
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("guestDetails");
		view.addObject("message", "Good morning From Controller");
		view.addObject("guestInfo",guest);
		return view;
	}
}
