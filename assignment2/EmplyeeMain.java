package friday.assignment2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class EmplyeeMain {

	private static Scanner sc;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		System.out.println("Enter number of Employees:=");
		int noOfEmployees=sc.nextInt();
		
		EmployeeVo[] employeeVos=new EmployeeVo[noOfEmployees];
		EmployeeBo ob=new EmployeeBo();
		for (int i = 0; i < noOfEmployees; i++)
		{
		
			employeeVos[i]=new EmployeeVo();
			
			System.out.println("enter the details of the employee");
			System.out.println("enter the employeeId:= ");
			employeeVos[i].setEmployeeId(sc.nextInt());
			System.out.println("enter the empname:= ");
			employeeVos[i].setEmpname(sc.next());
			System.out.println("enter the annualIncome:= ");
			employeeVos[i].setAnnualIncome(sc.nextInt());
			ob.calincomeTax(employeeVos[i]);						
		}
		System.out.println("\nEmployees:");
		display(employeeVos);
		
		System.out.println("\nEmployees after sorting IT in descending order:");

		Collections.sort(Arrays.asList(employeeVos), new EmplyeeSort());
		display(employeeVos);
		
		
	}
	public static void display(EmployeeVo[] emp) {
		for(EmployeeVo e: emp) {
			System.out.println(e);
		}
	}


}
