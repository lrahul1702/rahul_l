package com.MyMaven;

import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

public class AppGetTheData 
{
    public static void main( String[] args )
    {
    	Customer customer=new Customer();
    	
    	AnnotationConfiguration configuration = new AnnotationConfiguration().configure();
    	
    	SessionFactory factory = configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	
    	customer=(Customer) session.get(Customer.class, 14);
    	customer.setCustomerName("ganga");
    	
    	System.out.println(customer);
    	
    	transaction.commit();
    	System.out.println("Data Stored");
    	session.close();
    	factory.close();
		
    }
}
