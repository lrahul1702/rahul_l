package com.MyMaven;

import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

public class AppDelete 
{
    public static void main( String[] args )
    {
    	Customer customer=new Customer();
    	customer.setCustomerId(15);
    	
    	AnnotationConfiguration configuration = new AnnotationConfiguration().configure();
    	
    	SessionFactory factory = configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	
    	session.delete(customer);
    	
    	transaction.commit();
    	System.out.println("Data deleted");
    	session.close();
    	factory.close();
		
    }
}
