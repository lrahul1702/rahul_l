package com.MyMaven;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.javatraining.customer.model.Customer;

public class AppSelectAll 
{
    public static void main( String[] args )
    {
    	Customer customer=new Customer();
    	
    	AnnotationConfiguration configuration = new AnnotationConfiguration().configure();
    	
    	SessionFactory factory = configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	System.out.println("All Customer Details: ");

    	/*Query q=session.createQuery("from Customer");*/
    	Criteria q=session.createCriteria(Customer.class).add(Restrictions.eq("customerId",14));
    	
    	List<Customer> customers=q.list();
    	
    	Iterator<Customer>iterator=customers.iterator();
    	while(iterator.hasNext())
    	{
    		Customer cust=iterator.next();
    		System.out.println(cust);
    	}
    	  	

    	session.close();
    	factory.close();
		
    }
}
