package com.MyMaven;

import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

public class App 
{
    public static void main( String[] args )
    {
    	Customer customer=new Customer(1232, "Ram", "Agra", 223);
    	
    	@SuppressWarnings("deprecation")
		AnnotationConfiguration configuration = new AnnotationConfiguration().configure();
    	
    	@SuppressWarnings("deprecation")
		SessionFactory factory = configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	session.save(customer);
    	transaction.commit();
    	System.out.println("Data Stored");
    	session.close();
    	factory.close();
		
    }
}
