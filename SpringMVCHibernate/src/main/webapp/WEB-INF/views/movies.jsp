<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Read Stories</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- google fonts -->

		<!-- Css link -->
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="resources/show/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/show/css/owl.carousel.css">
		<link rel="stylesheet" href="resources/show/css/owl.transitions.css">
		<link rel="stylesheet" href="resources/show/css/animate.min.css">
		<link rel="stylesheet" href="resources/show/css/lightbox.css">
		<link rel="stylesheet" href="resources/show/css/bootstrap.min.css">
		<link rel="stylesheet" href="resources/show/css/preloader.css">
		<link rel="stylesheet" href="resources/show/css/image.css">
		<link rel="stylesheet" href="resources/show/css/icon.css">
		<link rel="stylesheet" href="resources/show/css/style.css">
		<link rel="stylesheet" href="resources/show/css/responsive.css">

	</head>
	<body id="top">
	
 		<section id="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title">
							<h2>LATEST WORKS</h2>
							<p>Read finest stories from our StoryBoat Community</p>
						</div>
						<div class="block">
							<div class="recent-work-mixMenu">
								<ul>
									<li><button class="filter" data-filter="all">All</button></li>
									<li><button class="filter" data-filter=".category-1">New And Trending</button></li>
									<li><button class="filter" data-filter=".category-2">Upcoming</button></li>
									<li><button class="filter" data-filter=".category-3">Hits</button></li>
									<li><button class="filter" data-filter=".category-4">English </button></li>
									<li><button class="filter" data-filter=".category-5">Hindi</button></li>
									<li><button class="filter" data-filter=".category-6">Telugu</button></li><br><hr>
									
														 
														 
								</ul>
							</div>
                           
                         
							<div class="recent-work-pic">
                                 <form name="nameform">
								<ul id="mixed-items">
									
                                    <li class="mix category-4 col-md-4 col-xs-6" data-my-order="1">
                                      <form action="">  <h3>Gone Girl</h3>
                                       <a href="booking.do" onclick="name1()"> <img src="resources/show/images/English/gonegirl.jpg"  height="400" width="300" /></a>
										</form>
									</li>
                                   
                                    <li class="mix category-1 category-4 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>The Nun</h3>
                                        <a href="booking.do" onclick="name2()"> <img src="resources/show/images/English/nun.jpg"  height="400" width="300" /></a>
										
									</li>
                                    <li class="mix category-1 category-5 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>Razzi</h3>
                                        <a href="booking.do" onclick="name3()"> <img src="resources/show/images/hindi/raazi.jpg"  height="400" width="300" /></a>
										
									</li>
                                    <li class="mix category-1 category-5 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>dhadhak</h3>
                                        <a href="booking.do" onclick="name4()"> <img src="resources/show/images/hindi/dhadak.jpg"  height="400" width="300" /></a>
									</li>
                                                               
                                    <li class="mix category-3 category-5 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>bhoot</h3>
                                        <a href="booking.do" onclick="name6()"><img src="resources/show/images/hindi/bhoot.jpg"  height="400" width="300" /></a>
										
									</li>
                                    <li class="mix category-2 category-5 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>Kahani</h3>
                                        <a href="#" onclick="remark()">  <img src="resources/show/images/hindi/kahani.jpg"  height="400" width="300" /></a>
										
									</li>
                                    <li class="mix category-1 category-5 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>Pari</h3>
                                        <a href="booking.do" onclick="name7()"> <img src="resources/show/images/hindi/Pari.jpg"  height="400" width="300" /></a>
										
									</li>
                                   
                                    
                                    
                                    
                                    
                                    
                                    <li class="mix category-3 category-4 col-md-4 col-xs-6" data-my-order="1">
										  <h3>Inceprion</h3>
                                        <a href="booking.do" onclick="name8()">   <img src="resources/show/images/English/inception.jpg"  height="400" width="300" /></a>
                                        
									</li> 
                                    <li class="mix category-3 category-4 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>Twilight</h3>
                                        <a href="booking.do" onclick="name9()">  <img src="resources/show/images/English/twilight.jpg"  height="400" width="300" /></a>
										
									</li>
                                    
                                    
                                    
                                    
                                    <li class="mix category-4 col-md-4 col-xs-6" data-my-order="1">
										 
								
                                        <h3>Sara's Notebook</h3>
                                        <a href="booking.do" onclick="name10()">  <img src="resources/show/images/English/sara's%20notebook.jpg"  height="400" width="300" /></a>
										
									</li>
                                    <li class="mix category-4 col-md-4 col-xs-6" data-my-order="1">
                                        <h3>Conjuring</h3>
                                        <a href="booking.do" onclick="name11()">  <img src="resources/show/images/English/conjuring.jpg"  height="400" width="300" /></a>
										
									</li>
                                        
				 
                                    <li class="mix category-6 col-md-4 col-xs-6" data-my-order="1">
										 
											<h3>gruham</h3>
                                        <a href="booking.do" onclick="name12()">  <img src="resources/show/images/Telugu/gruham.jpg"  height="400" width="300" />	</a>
                                        
									</li>
                                    
                                    
                                    
                                    <li class="mix category-6 category-1 category-3 col-md-4 col-xs-6" data-my-order="1">
										 
											<h3>LOVELY</h3>
                                        <a href="booking.do" onclick="name13()"><img src="resources/show/images/Telugu/LOVELY.jpg"  height="400" width="300" /></a>	
                                        
								
                                    </li> 
                                <li class="mix category-6 category-1 col-md-4 col-xs-6" data-my-order="1">
										 
											<h3>Pisachi</h3>
                                    <a href="booking.do" onclick="name14()">  <img src="resources/show/images/Telugu/Pisachi.jpg"  height="400" width="300" /></a>	
                                        
								
                                    </li> 


			 
								</ul>
								</form>  
							</div>
                            <br><br>
                            	 
                   
                            
						</div>
					</div>
				</div>
			</div>
		</section>
		
		
 		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="block">
							 
							<p>All rights reserved © MoviesApp 2018</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

		<!-- load Js -->
		<script src="resources/show/js/jquery-1.11.3.min.js"></script>
		<script src="resources/show/js/bootstrap.min.js"></script>
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false"></script>
		<script src="resources/show/js/waypoints.min.js"></script>
		<script src="resources/show/js/lightbox.js"></script>
		<script src="resources/show/js/jquery.counterup.min.js"></script>
		<script src="resources/show/js/owl.carousel.min.js"></script>
		<script src="resources/show/js/html5lightbox.js"></script>
		<script src="resources/show/js/jquery.mixitup.js"></script>
		<script src="resources/show/js/wow.min.js"></script>
		<script src="resources/show/js/jquery.scrollUp.js"></script>
		<script src="resources/show/js/jquery.sticky.js"></script>
		<script src="resources/show/js/jquery.nav.js"></script>
		<script src="resources/show/js/main.js"></script>
		<script>
		function remark() {
			alert("Booking To Be Available Soon!")
			
		}</script>
		<script>
		function name1() {
			var b = "Gone Girl"
			url = 'http://path_to_your_html_files/next.html?name=' + encodeURIComponent(b);

		    document.location.href = url;
			
		}</script>
	</body>
</html>