package com.training.spring.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Booking")
public class Booking {
	
	private int userName;
	private int seatNo;
	private int movieName;
	private int billAmount;
	
	public int getUserName() {
		return userName;
	}
	public void setUserName(int userName) {
		this.userName = userName;
	}
	
	public int getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(int seatNo) {
		this.seatNo = seatNo;
	}
	public int getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(int billAmount) {
		this.billAmount = billAmount;
	}
	public int getMovieName() {
		return movieName;
	}
	public void setMovieName(int movieName) {
		this.movieName = movieName;
	}
	@Override
	public String toString() {
		return "Booking [userName=" + userName + ", seatNo=" + seatNo + ", movieName=" + movieName + ", billAmount="
				+ billAmount + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billAmount;
		result = prime * result + movieName;
		result = prime * result + seatNo;
		result = prime * result + userName;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booking other = (Booking) obj;
		if (billAmount != other.billAmount)
			return false;
		if (movieName != other.movieName)
			return false;
		if (seatNo != other.seatNo)
			return false;
		if (userName != other.userName)
			return false;
		return true;
	}
	
	
	
	

}
