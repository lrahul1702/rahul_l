package com.training.spring.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.LoginDAO;
import com.training.spring.model.Login;

@Service
public class LoginServiceImpl implements LoginService {
	
	private LoginDAO loginDAO;

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}
	
	@Override
	@Transactional
	public boolean checkLogin(Login p) {
			return this.loginDAO.checkLogin(p);
	}

	@Override
	@Transactional	
	public int storeLogin(Login p) {
		
		return this.loginDAO.storeLogin(p);
	}

	
	

}
