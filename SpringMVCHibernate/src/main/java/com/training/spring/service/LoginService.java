package com.training.spring.service;

import com.training.spring.model.Login;

public interface LoginService {

	public boolean checkLogin(Login p);
	public int storeLogin(Login p);
	
}
