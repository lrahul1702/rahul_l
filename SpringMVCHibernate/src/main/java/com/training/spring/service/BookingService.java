package com.training.spring.service;

import java.util.List;

import com.training.spring.model.Booking;

public interface BookingService {
	
	public Booking getBookingByusername(String username);
	public List<Booking> listBooking();
	public Booking generateBillByBillAmount(int seatNo);

}
