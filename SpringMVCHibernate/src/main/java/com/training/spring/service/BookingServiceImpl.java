package com.training.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.BookingDAO;
import com.training.spring.model.Booking;

@Service
public class BookingServiceImpl implements BookingService {
	
	private BookingDAO bookingDAO;
	
	public void setBookingDAO(BookingDAO bookingDAO) {
		this.bookingDAO = bookingDAO;
	}

	@Override
	@Transactional
	public Booking getBookingByusername(String username) {
		return this.bookingDAO.getBookingByUserName(username);
	}
	

	@Override
	@Transactional
	public List<Booking> listBooking() {
		// TODO Auto-generated method stub
		return this.bookingDAO.listBooking();
	}

	@Override
	@Transactional
	public Booking generateBillByBillAmount(int seatNo) {
		// TODO Auto-generated method stub
		return this.generateBillByBillAmount(seatNo);
	}

	

	

}
