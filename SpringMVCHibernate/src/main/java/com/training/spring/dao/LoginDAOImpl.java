package com.training.spring.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.training.spring.model.Login;

@Repository
public class LoginDAOImpl implements LoginDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	@Override
	public boolean checkLogin(Login login) {
		System.out.println("Inside Login DAO Impl.."+login);
		Session session = this.sessionFactory.getCurrentSession();	
		//Login result = (Login) session.get(Login.class, login.getUserName());
		Criteria criteria =  session.createCriteria(Login.class)
					.add(Restrictions.eq("userName",login.getUserName()))
					.add(Restrictions.eq("password",login.getPassword()));
				
		List<Login> data = criteria.list();
		
		System.out.println("inside Login DAO Impl result is :"+data);
		if(data.size() == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	@Override
	public int storeLogin(Login p) {
		
			//System.out.println("Inside Login DAO Impl.."+login);
			Session session = this.sessionFactory.getCurrentSession();	
			//Login result = (Login) session.get(Login.class, login.getUserName());

			Criteria criteria =  session.createCriteria(Login.class)
						.add(Restrictions.eq("userName",p.getUserName()))
						.add(Restrictions.eq("password",p.getPassword()));
					
			List<Login> data = criteria.list();
			
			System.out.println("inside Login DAO Impl result is :"+data);
			if(data.size() == 0)
			{
				session.save(p);
				return 1;
			}
			else
			{
				return 0;
			}
	}
}
