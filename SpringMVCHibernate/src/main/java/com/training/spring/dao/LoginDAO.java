package com.training.spring.dao;

import com.training.spring.model.Login;

public interface LoginDAO {

	public boolean checkLogin(Login p);
	public int storeLogin(Login p);
	
}
