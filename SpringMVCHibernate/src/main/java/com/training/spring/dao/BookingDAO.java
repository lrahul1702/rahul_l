package com.training.spring.dao;

import java.util.List;

import com.training.spring.model.Booking;
import com.training.spring.model.Person;

public interface BookingDAO {
	
	public Booking getBookingByUserName(String username);
	public List<Booking> listBooking();
	public Booking generateBillByBillAmount(int seatNo);
	

}
