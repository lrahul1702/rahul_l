package com.training.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.training.spring.model.Booking;

@Repository
public class BookingDAOImpl implements BookingDAO {
	private static final Logger logger = LoggerFactory.getLogger(BookingDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	/*@Override
	public Booking getBookingByEmail(String emailId) {
		Session session = this.sessionFactory.getCurrentSession();
		Booking b=(Booking) session.load(Booking.class, new String(emailId));
		logger.info("Booking loaded successfully. Booking details= " +b);
		return b;
	}
*/
	
	@Override
	public List<Booking> listBooking() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Booking> bookingList=session.createCriteria("from Booking").list();
		for(Booking b : bookingList) {
			logger.info("Booking List:: " +b);
		}
		return bookingList;
	}
	
	@Override
	public Booking generateBillByBillAmount(int seatNo) {
		
		return null;
	}

	@Override
	public Booking getBookingByUserName(String username) {
		Session session = this.sessionFactory.getCurrentSession();
		Booking b=(Booking) session.load(Booking.class, new String(username));
		logger.info("Booking loaded successfully. Booking details= " +b);
		return b;
	}

}
