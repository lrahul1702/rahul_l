package com.training.spring;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.training.spring.model.Login;
import com.training.spring.service.LoginService;
import com.training.spring.service.PersonService;

@Controller
public class LoginController {
	
	private PersonService personService;
	private LoginService loginService;
	
	@Autowired(required=true)
	@Qualifier(value="loginService")
	public void setLoginService(LoginService ls){
		this.loginService = ls;
	}
	
	@RequestMapping(value={"/logins"})
	public String listPersons(Model model,Login login) {
		System.out.println("login details"+login);
		/*login.setUserName("ram");
		login.setPassword("kbc");*/
		/*System.out.println("AFTER SETTING login details"+login);*/
		boolean result = loginService.checkLogin(login);
		System.out.println(result);
		model.addAttribute("login", login);
		
		if(result==true)
		{
			return "movies";
		}
		else
		{
			return "signup";
		}
	}
	@RequestMapping(value={"/signup"})
	public String listPersons1(Login login) {
		/*System.out.println("login details"+login);
		System.out.println("AFTER SETTING login details"+login);*/
		int result = loginService.storeLogin(login);
		System.out.println(result);
		//model.addAttribute("login", login);
		if(result==1)
		{
			return "movies";
		}
		else
		{
			return "logins";
		}
		
	}
	@RequestMapping(value="/booking")
	public String listPersons2(Model model) {
		return "ticket";
	}
	@RequestMapping(value="/booking",method= RequestMethod.GET)
	public String listPersons3(Model model) {
		return "booking";
	}
	@RequestMapping(value="/bill")
	public String listPersons4(Model model) {
		return "bill";
	}
	/*//For add and update person both
	@RequestMapping(value= "/person/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") Person p){
		System.out.println("#####product :"+p);
			this.personService.addPerson(p);
		return "redirect:/persons";
	}

	@RequestMapping(value= "/edit/add/update", method = RequestMethod.POST)
	public String updatePerson(@ModelAttribute("person") Person p){
		System.out.println("#####product updating :"+p);
			this.personService.updatePerson(p);
		return "redirect:/persons";
	}
	
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
    @RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") int id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
        model.addAttribute("listPersons", this.personService.listPersons());
        return "person";
    }*/
	
}
