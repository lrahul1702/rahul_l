package com.javatraining.customer.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.javatraining.customer.dbcon.DBConfig;
import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	@Override
	public int insertCustomer(Customer customer) {
		Connection connection=DBConfig.getConnection();
		int row=0;
		String query="insert into customer values(?,?,?,?)";
		PreparedStatement statement;
		try
		{
			statement = connection.prepareStatement(query);
			statement.setInt(1, customer.getCustomerId());
			statement.setString(2, customer.getCustomerName());
			statement.setString(3, customer.getCustomerAddress());
			statement.setInt(4, customer.getBillAmount());
			row=statement.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		try 
		{
			connection.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return row;
		
	}

	@Override
	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount) {
		
		Connection connection=DBConfig.getConnection();
		int row=0;
		String query="update customer set customerAddress=?,billamount=? where customerId=?";
		try
		{
			PreparedStatement statement=connection.prepareStatement(query);
			statement.setInt(3, customerId);
			statement.setString(1, newCustomerAddress);
			statement.setInt(2, newBillAmount);
			row=statement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
			
		return row;
	}

	@Override
	public int deleteCustomer(int customerId) {
		
		Connection connection=DBConfig.getConnection();
		int row=0;
		String query=" delete from customer where customerId=?";
		try
		{
			PreparedStatement statement=connection.prepareStatement(query);
			statement.setInt(1, customerId);
			row=statement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
			
		return row;
	}

	@Override
	public Customer findByCustomerId(int customerId) {
		Connection connection=DBConfig.getConnection();
		String query="select * from customer where customerId=?";
		try
		{
			PreparedStatement statement=connection.prepareStatement(query);
			statement.setInt(1, customerId);
			ResultSet resultSet=statement.executeQuery(query);
			if(resultSet.next())
			{
				Customer customer=new Customer();
				customer.setCustomerId(resultSet.getInt(1));
				customer.setCustomerName(resultSet.getString(2));
				customer.setCustomerAddress(resultSet.getString(3));
				customer.setBillAmount(resultSet.getInt(4));
				return customer;
			}
			else
				return null;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public boolean isCustomerExists(int customerId) {
		
		Connection connection=DBConfig.getConnection();
		String query="select * from customer where customerId=?";
		ResultSet resultSet;
		try
		{
			PreparedStatement statement=connection.prepareStatement(query);
			statement.setInt(1, customerId);
			resultSet = statement.executeQuery(query);
			if(resultSet.next())
			{
				return true;
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Customer> listAllCustomers() {

		List<Customer> allCustomer=new ArrayList<Customer>();
		Connection connection=DBConfig.getConnection();
		String query="select * from customer";
		try
		{
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery(query);
			while(resultSet.next())
			{
				Customer customer=new Customer();
				customer.setCustomerId(resultSet.getInt(1));
				customer.setCustomerName(resultSet.getString(2));
				customer.setCustomerAddress(resultSet.getString(3));
				customer.setBillAmount(resultSet.getInt(4));
				allCustomer.add(customer);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return allCustomer;
	}

}
