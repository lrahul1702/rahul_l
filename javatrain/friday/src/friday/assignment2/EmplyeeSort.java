package friday.assignment2;

import java.util.Comparator;

public class EmplyeeSort implements Comparator<EmployeeVo> {

	@Override
	public int compare(EmployeeVo o1, EmployeeVo o2) {
		if(o1.getIncomeTax()<o2.getIncomeTax())
			return 1;
		else
			return -1;
	}

	
}
