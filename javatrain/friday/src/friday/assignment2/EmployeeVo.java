package friday.assignment2;

public class EmployeeVo {

	int employeeId;
	String empname;
	int annualIncome;
	int incomeTax;
	
	public EmployeeVo() {
	
	}

	public EmployeeVo(int employeeId, String empname, int annualIncome, int incomeTax) {
		super();
		this.employeeId = employeeId;
		this.empname = empname;
		this.annualIncome = annualIncome;
		this.incomeTax = incomeTax;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public int getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(int annualIncome) {
		this.annualIncome = annualIncome;
	}

	public int getIncomeTax() {
		return incomeTax;
	}

	public void setIncomeTax(int incomeTax) {
		this.incomeTax = incomeTax;
	}

	@Override
	public String toString() {
		return "EmployeeVo [employeeId=" + employeeId + ", empname=" + empname + ", annualIncome=" + annualIncome
				+ ", incomeTax=" + incomeTax + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + annualIncome;
		result = prime * result + employeeId;
		result = prime * result + ((empname == null) ? 0 : empname.hashCode());
		result = prime * result + incomeTax;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (annualIncome != other.annualIncome)
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (empname == null) {
			if (other.empname != null)
				return false;
		} else if (!empname.equals(other.empname))
			return false;
		if (incomeTax != other.incomeTax)
			return false;
		return true;
	}


	
	
}
