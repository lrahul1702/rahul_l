package com.javatraining.customer.client;

import java.util.ArrayList;
import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {

		Customer customer = new Customer(999, "Shahrukh", "Mumbai", 99000);
		CustomerDAO customerDAO=new CustomerDAOImpl();
		
		int result=customerDAO.insertCustomer(customer);
		System.out.println(result+"row affected");
		
		int customerId=5;
		String newCustomerAddress="usa";
		int newBillAmount=234;
		
		result=customerDAO.updateCustomer(customerId,newCustomerAddress ,newBillAmount);
		
		List<Customer> allcustomers=new ArrayList<Customer>();
		allcustomers=customerDAO.listAllCustomers();
		System.out.println(allcustomers);
		
		
		
	}

}
