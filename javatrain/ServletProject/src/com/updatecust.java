package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class updatecust
 */
public class updatecust extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updatecust() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int customerID=Integer.parseInt(request.getParameter("customerId"));

		CustomerDAO customerDAO=new CustomerDAOImpl();
		boolean res =customerDAO.isCustomerExists(customerID);
		if (res)
		{
			response.getWriter().println("alert('enter valid customr id')");
			
		}
		else
		{
			String newCustomerAddress=request.getParameter("newcustomerAddress");
			int newBillAmount=Integer.parseInt(request.getParameter("newbillAmount"));
			int result=customerDAO.updateCustomer(customerID,newCustomerAddress,newBillAmount);
			response.getWriter().println("<h1>rows updated= "+result+"</h1>");
		}		
		response.getWriter().println("<a href=\"enter.html\">enter</a>");
		
	}

}
