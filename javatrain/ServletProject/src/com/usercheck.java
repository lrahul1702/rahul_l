package com;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class usercheck
 */
public class usercheck extends HttpServlet {
	
	int counter=0;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public usercheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		String uname=request.getParameter("uname");
		String upass=request.getParameter("upass");
		
		if(uname.equalsIgnoreCase("rahul"))
		{
			HttpSession session=request.getSession();
			session.setAttribute("username", uname);
			
			RequestDispatcher dispatcher=request.getRequestDispatcher("Enter");
			dispatcher.include(request, response);
			
			counter++;
			response.getWriter().println("<h1>welcome in our web site "+uname+"</h1>");
			response.getWriter().println("<font color='green'><h1>you are visitor number:"+counter);
			response.getWriter().println("<a href=\"enter.html\">enter</a>");
		}
		
		else
		{
			response.sendRedirect("userlogin.html");
		}
		
		
	}
}
