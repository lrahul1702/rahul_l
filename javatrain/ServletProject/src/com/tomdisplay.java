package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class tomdisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public tomdisplay() {
        super();
    }
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name=request.getParameter("uname");
		String color[]=request.getParameterValues("colors");
		boolean alreadyVisited=false;
		Cookie allCookies[]=request.getCookies();
		if(allCookies!=null)
		{
			for(Cookie c:allCookies)
			{
				if(c.getName().equals(name))
				{
					alreadyVisited=true;
					break;
				}
			}
			
		
		}
		if(alreadyVisited)
		{
			response.getWriter().println("<h1>you have already visited-"+name+"</h1>");
		}
		else
		{
			response.getWriter().println("<h1>you are first time visitor-"+name+"</h1>");
			Cookie cookie=new Cookie(name,name);
			response.addCookie(cookie);
		}
		if(color==null)
		{
			response.getWriter().println("<h1>you have not selected any color</h1>");
			
		}
		else
		{
			
			for(String c:color)
			{
				response.getWriter().println("<h1><font color="+c+">Hello "+name+"</h1>");
			}
			
		}
	
		
		
	}

}
