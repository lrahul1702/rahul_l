package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class CustomerInfo
 */
public class CustomerInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int customerID=Integer.parseInt(request.getParameter("customerId"));
		String customerName=request.getParameter("customerName");
		String customerAddress=request.getParameter("customerAddress");
		int billAmount=Integer.parseInt(request.getParameter("billAmount"));
		response.getWriter().println("<h1>customerID= "+customerID+"</h1>");
		response.getWriter().println("<h1>customerName= "+customerName+"</h1>");
		response.getWriter().println("<h1>customerAddress= "+customerAddress+"</h1>");
		response.getWriter().println("<h1>billAmount= "+billAmount+"</h1>");
		response.getWriter().println("<a href=\"enter.html\">enter</a>");
		response.getWriter().println("<a href=\"update.html\">update</a>");
		response.getWriter().println("<a href=\"delete.html\">delete</a>");
		
		HttpSession session=request.getSession();
		session.setAttribute("username", customerName);
		
		Customer customer = new Customer(customerID, customerName, customerAddress, billAmount);
		CustomerDAO customerDAO=new CustomerDAOImpl();
		int result=customerDAO.insertCustomer(customer);
		
	/*	response.setContentType("text/html");
		response.getWriter().println("id:"+customerID
				+"<br> name:"+customerName
				+"<br> address:"+customerAddress
				+"<br> billamount:"+billAmount);*/
	}
		
}
