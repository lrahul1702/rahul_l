<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="b" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="a" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a:setDataSource var="ds" driver="oracle.jdbc.driver.OracleDriver" url="jdbc:oracle:thin:@:1521:orcl"
scope="session" user="scott" password="tiger"/>
<a:query var="q" dataSource="ds">
select customerId,customerName,customerAddress,billAmount from customer
</a:query>
<table border=1>
<b:forEach var="row" items="${q.rows}">
<tr>
<td><b:out value="${row.customerId}"/></td>
<td><b:out value="${row.customerName}"/></td>
<td><b:out value="${row.customerAddress}"/></td>
<td><b:out value="${row.billAmount}"/></td>
</tr>
</b:forEach>
</table>
</body>
</html>


