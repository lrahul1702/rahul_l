package client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.model.Customer;
import com.javatraining.customer.model.Email;

@SuppressWarnings("deprecation")
public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	/*	Customer customer=new Customer();
		customer.setCustomerName("rahul");
		System.out.println(customer.getCustomerName());*/
		
		Resource resource=new ClassPathResource("beans.xml");
		@SuppressWarnings("deprecation")
		BeanFactory factory=new XmlBeanFactory(resource);
		
		/*Customer customer1=(Customer) factory.getBean("cust1");
		System.out.println(customer1);
		
		Customer customer2=(Customer) factory.getBean("cust2");
		System.out.println(customer2);*/
		
		Email email= (Email) factory.getBean("email");
		System.out.println(email);
		
		
	}

}
