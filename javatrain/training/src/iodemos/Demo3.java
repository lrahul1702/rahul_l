package iodemos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Demo3 {

	public static void main(String[] args) throws IOException {

		File file=new File("c:\\training\\hello.txt");
		FileInputStream stream=new FileInputStream(file);
		
		int i=0;
		while ((i=stream.read())!=-1) 
		{
			System.out.print((char)i);
			
		}
		stream.close();
		
	}

}
