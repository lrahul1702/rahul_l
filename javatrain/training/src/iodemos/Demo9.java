package iodemos;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Demo9 {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		Customer c1=new Customer(123, "jamesbond","kakinada", 0);
		ObjectOutputStream stream=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("cust.txt")));
		
		stream.writeObject(c1);
		stream.close();
		System.out.println("customer record stored");
		
	}

}
