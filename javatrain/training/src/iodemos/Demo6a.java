package iodemos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo6a {

	public static void main(String[] args) throws IOException {
	

		BufferedReader stream=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("please enter your name:");
		String name=stream.readLine();
		
		System.out.println("please enter your marks:");
		int marks=Integer.parseInt(stream.readLine());
		
		System.out.println(name+"scored"+marks);
		
		stream.close();
	}

}
