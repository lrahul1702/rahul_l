package iodemos;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Demo5a {

	public static void main(String[] args) throws IOException {

		BufferedInputStream stream=new BufferedInputStream(new FileInputStream(new File("c:\\training\\hello.txt")));
		int i=0;
		while ((i=stream.read())!=-1) 
		{
			System.out.print((char)i);
			
		}
		stream.close();
	}

}
