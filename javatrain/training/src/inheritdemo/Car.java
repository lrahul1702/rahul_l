package inheritdemo;

public abstract class Car extends Vehicle {
	String carType;
	
	public void start() {
		System.out.println("car started");
	}
	abstract public void kickstart();
	public void showDetails()
	{
		color="red";
		noOfWheels=4;
		carType="hatchback";
		
		System.out.println("color="+color);
		System.out.println("noofwheels="+noOfWheels);
		System.out.println("cartype="+carType);
	}
	
}
