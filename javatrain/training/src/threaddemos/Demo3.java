package threaddemos;

public class Demo3 extends Thread {

	static int count=0;
	public static synchronized void display(String threadName,String message1,String message2)
	{
		System.out.println("message by:"+threadName);
		System.out.println("message1"+threadName+" is:"+message1);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("message2"+threadName+" is:"+message2);
		
	}
	public Demo3() 
	{
		super(" "+count++);
		start();
		
	}
	
	@Override
	public void run() {
		
		display(Thread.currentThread().getName(),"hi","bye");
		
//		System.out.println("run called by"+Thread.currentThread().getName());
		
	}	
	public static void main(String[] args) {
		new Demo3();
		new Demo3();
		new Demo3();
		new Demo3();
		//System.out.println("run called by"+Thread.currentThread().getName());

	}

}
