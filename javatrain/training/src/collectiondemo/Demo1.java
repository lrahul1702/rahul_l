package collectiondemo;

import java.util.*;
public class Demo1 {

	public static void main(String[] args) {

		List<String> batchList=new ArrayList<String>();
		
		
		batchList.add("kapoor");
		batchList.add("kumar");
		batchList.add("sharma");
		batchList.add("kumar");
		batchList.add("sharma");
		batchList.add("kapoor");
		
		batchList.remove("kumar");
		
		System.out.println(batchList);
		
		Iterator<String> i=batchList.iterator();
		
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
	}

}
