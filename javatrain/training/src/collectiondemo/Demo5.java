package collectiondemo;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Demo5 {

	public static void main(String[] args) {
		
		List<String> all=new ArrayList<String>();
		all.add("anu");
		all.add("anil");
		all.add("aku");
		all.add("nanu");
		all.add("annnu");
		
		int pos=Collections.binarySearch(all, "aku");
		 
		System.out.println(pos);
		
	

	}

}
