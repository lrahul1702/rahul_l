package collectiondemo;

public class Generic2Parameter <Z,Y>{

	public void display(Z s,Y y)
	{
		System.out.println(s+" "+y);
	}
	public static void main(String[] args) {
	
		Generic2Parameter<Boolean,String> hello= new Generic2Parameter<Boolean,String>();
		hello.display(true,"always");
		
		Generic2Parameter<String,Integer> hello2= new Generic2Parameter<String,Integer>();
		hello2.display("rahul",007);
	}
	
}
