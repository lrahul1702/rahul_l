package collectiondemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
//import java.util.Iterator;
import java.util.List;

public class Demo2 {

	public static void main(String[] args) {
	
		Customer customer1=new Customer(1, "rahul", "delhi", 23022);
		Customer customer2=new Customer(2, "netu", "agra", 2313);	
		Customer customer3=new Customer(3, "ramal", "goa", 232);
		Customer customer4=new Customer(4, "kmdas", "pune", 230);
		Customer customer5=new Customer(5, "rao", "delhi", 2233);
		
		List<Customer> allcustomer=new ArrayList<Customer>();
		allcustomer.add(customer1);
		allcustomer.add(customer2);
		allcustomer.add(customer3);
		allcustomer.add(customer4);
		allcustomer.add(customer5);
		
		System.out.println(allcustomer);
		
		Collections.sort(allcustomer,new AddressComparator());
		
		System.out.println("after sortingby address.");
		
		System.out.println(allcustomer);
		
		Collections.sort(allcustomer,new BillAmountComparator());
		
		System.out.println("after sortingby billamount.");
		
		System.out.println(allcustomer);
		
		Collections.sort(allcustomer,new Comparator<Customer>()
				{
					public int compare(Customer o1,Customer o2)
					{
						if(o1.getCustomerId()<o2.getCustomerId())
							return 1;
						else 
							return -1;
					}
				});
		
		System.out.println("after sortingby customer id.");
		
		System.out.println(allcustomer);
		
		
	
	}

}
