package collectiondemo;

public class GenericAddition<T>{

	public <P extends Number> double gett(P s,P y)
	{
	
		return(s.doubleValue()+y.doubleValue());
	}
	public static void main(String[] args) {
	
		GenericAddition<Integer> hello= new GenericAddition<Integer>();
		System.out.println(hello.gett(23,45));
		
		GenericAddition<Double> hello2= new GenericAddition<Double>();
		System.out.println(hello2.gett(234.344,007.23));
	}
	
		
		
}
