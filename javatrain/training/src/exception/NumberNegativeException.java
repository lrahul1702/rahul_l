package exception;

public class NumberNegativeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberNegativeException() {
		}

	public NumberNegativeException(String message) {
		super(message);
	}
	
}
