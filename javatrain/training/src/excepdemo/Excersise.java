package excepdemo;

public class Excersise {

	public static void main(String[] args) {
		String a="The quick brown fox jumps over the lazy dog ";
		System.out.println("1.	Print the character at the 12th index. "+a.charAt(12));
		System.out.println("2.	Check whether the String contains the word �is�."+a.contains("is"));
		System.out.println("3.	Add the string �and killed it� to the existing string. "+a.concat("and killed it"));
		System.out.println("4.	Check whether the String ends with the word dogs."+a.endsWith("dog"));
		System.out.println("5.	Check whether the String is equal to /�The quick brown Fox jumps over the lazy Dog/�."+a.equals("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("6.	Check whether the String is equal to �THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�." +a.toUpperCase());
		System.out.println("7.	Find the length of the String.="+a.length());
		System.out.println("8.	Check whether the String matches to /�The quick brown Fox jumps over the lazy Dog/�.= "+a.compareTo("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("9.	Replace the word /�The/� with the word /�A/�."+a.replace("The", "a"));
		String[] b = a.split("over");
		//System.out.println(b[]);
		System.out.println("10.	Split the above string into two such that two animal names do not come together."+b);
		System.out.println("11.	Print the animal names alone separately from the above string. "+a.substring(16,19)+" "+a.substring(40, 44));
		System.out.println("12.	Print the above string in completely lower case. "+a.toLowerCase());
		System.out.println("13.	Print the above string in completely upper case."+a.toUpperCase());
		System.out.println("14.	Find the index position of the character �a�"+a.indexOf("a"));
		System.out.println("15.	Find the last index position of the character �e�."+a.lastIndexOf("e"));
		
		
		
	}
	

}
