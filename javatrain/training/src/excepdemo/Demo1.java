package excepdemo;

import java.util.Scanner;

import exception.NumberNegativeException;

public class Demo1 {

	int num1,num2,result;	
	Scanner sc =new Scanner(System.in);
	public void display()
	{
		System.out.println("enter first number:");
		num1=sc.nextInt();
		try 
		{
				if (num1<0)
				{
					throw new NumberNegativeException("what are you doing?");
				}
		} 
		catch (NumberNegativeException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("enter second number:");
		num2=sc.nextInt();
		try {
			result=num1/num2;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("cant divide by 0.");
		}
		System.out.println("the result is"+result);
		
	}
	public static void main(String[] args) {
		
		Demo1 o=new Demo1();
		o.display();
		System.out.println("thank you for using the program.");

	}

}
