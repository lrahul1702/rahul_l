package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.java.dbcon.DBConfig;

public class Exercise {

	public static void main(String[] args) throws SQLException {



		Scanner sc= new Scanner(System.in);
		System.out.println("enter customer id to update :=");
		int customerId=sc.nextInt();
		System.out.println("enter new address:=");
		String customerAddress=sc.next();
		System.out.println("enter new bill amount:=");
		int customerBill=sc.nextInt();
		Connection connection=DBConfig.getConnection();
		PreparedStatement statement=connection.prepareStatement("update customer set customerAddress=?,billamount=? where customerId=?");
		statement.setInt(3, customerId);
		statement.setString(1, customerAddress);
		statement.setInt(2, customerBill);
		
		int row=statement.executeUpdate();
		System.out.println(row+"data updated");
		
		
	}

}
