package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.java.dbcon.DBConfig;

public class NewExcercise {

	private static Scanner sc;

	public static void main(String[] args) throws SQLException {

		Connection connection=DBConfig.getConnection();
		sc = new Scanner(System.in);
		System.out.println("enter customer id to update :=");
		int customerId=sc.nextInt();
		
		PreparedStatement statement=connection.prepareStatement("select * from customer where customerId=?");
		statement.setInt(1, customerId);
		ResultSet res=statement.executeQuery();
		if(res.next())
		{
			String name=res.getString(2);
			System.out.println("welcome "+name);
			System.out.println("enter new address:=");
			String customerAddress=sc.next();
			System.out.println("enter new bill amount:=");
			int customerBill=sc.nextInt();
			statement=connection.prepareStatement("update customer set customerAddress=?,billamount=? where customerId=?");
			statement.setInt(3, customerId);
			statement.setString(1, customerAddress);
			statement.setInt(2, customerBill);
			if(statement.executeUpdate()>0) 
			{
				System.out.println("congrats your record is updated");

			}
			PreparedStatement statement1=connection.prepareStatement("select * from customer where customerId=?");
			statement1.setInt(1, customerId);
			ResultSet res1 = statement1.executeQuery();
			if(res1.next())
			{
				name=res1.getString(2);
				String address=res1.getString(3);
				int bill=res1.getInt(4);
				System.out.println("congrats "+name+".your updated address := "+address+" and billamount := "+bill);
			}
			statement1.close();		
		}
		else
		{
			System.out.println("customer id="+customerId+" doesn't exist");
		}

		res.close();
		statement.close();
		connection.close();

		
		
		
		
		
	}

}
