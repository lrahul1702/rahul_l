package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.java.dbcon.DBConfig;

public class DeleteCustomerData {

	private static Scanner sc;

	public static void main(String[] args) throws SQLException {

		
		sc = new Scanner(System.in);
		int customerId=sc.nextInt();
				
		Connection connection=DBConfig.getConnection();
		PreparedStatement statement=connection.prepareStatement("select * from customer where customerId=? ");
		statement.setInt(1, customerId);
		
		ResultSet res=statement.executeQuery();
		if(res.next())
		{
			PreparedStatement statement2=connection.prepareStatement("delete from customer where customerId=?");
			statement2.setInt(1,customerId);
			
			int rows=statement2.executeUpdate();
			
			System.out.println(rows+"affected");
		}
		else
		{
			System.out.println("customer with customerId "+customerId+"doesnt exist");
		}
	}

}
