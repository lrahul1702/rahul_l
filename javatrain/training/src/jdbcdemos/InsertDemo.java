package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.java.dbcon.DBConfig;

public class InsertDemo {

	private static Scanner sc;

	public static void main(String[] args) throws SQLException {

		sc = new Scanner(System.in);
		System.out.println("enter customer id:=");
		int customerId=sc.nextInt();
		System.out.println("enter customer name:=");
		String customerName=sc.next();
		System.out.println("enter customer address:=");
		String customerAddress=sc.next();
		System.out.println("enter customer bill amount:=");
		int customerBill=sc.nextInt();
		PreparedStatement statement = insertCustomerDetails(customerId, customerName, customerAddress, customerBill);
		
		int row=statement.executeUpdate();
		System.out.println(row+"data inserted");
		
		
		
	}

	public static PreparedStatement insertCustomerDetails(int customerId, String customerName, String customerAddress,
			int customerBill) throws SQLException {
		Connection connection=DBConfig.getConnection();
		PreparedStatement statement=connection.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, customerId);
		statement.setString(2, customerName);
		statement.setString(3, customerAddress);
		statement.setInt(4, customerBill);
		return statement;
	}
	
	public static PreparedStatement insertCustomerDetails(Customer customer) throws SQLException
	{
		Connection connection=DBConfig.getConnection();
		PreparedStatement statement=connection.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, customer.getBillAmount());
		statement.setString(2, customer.getCustomerName());
		statement.setString(3, customer.getCustomerAddress());
		statement.setInt(4, customer.getBillAmount());
		return statement;
	}

}
