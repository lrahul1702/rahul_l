package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.java.dbcon.DBConfig;

public class DemoUser {

	private static Scanner sc;

	public static void main(String[] args) throws SQLException {

		Connection connection=DBConfig.getConnection();
		sc = new Scanner(System.in);
		System.out.println("enter username:=");
		String user=sc.next();
		System.out.println("enter password:=");
		String pass=sc.next();
		
		PreparedStatement statement=connection.prepareStatement("select * from user1 where username=? and password=?");
		statement.setString(1,user);
		statement.setString(2,pass);
		ResultSet res=statement.executeQuery();
		if(res.next())
		{
			System.out.println("welcome "+user);
		}
		else
		{
			System.out.println("invalid user");
		}

		res.close();
		statement.close();
		connection.close();
	}

}
