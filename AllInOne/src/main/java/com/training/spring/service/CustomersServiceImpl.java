package com.training.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.CustomerDAO;
import com.training.spring.dao.PersonDAO;
import com.training.spring.model.Customers;
import com.training.spring.model.Person;

@Service
public class CustomersServiceImpl implements CustomersService {
	
	private CustomerDAO customerDAO;

	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}
	@Override
	@Transactional
	public void addCustomers(Customers c) {
		this.customerDAO.addCustomer(c);		
	}

	@Override
	@Transactional
	public void updateCustomers(Customers c) {
		this.customerDAO.updateCustomer(c);
		
	}

	@Override
	@Transactional
	public List<Customers> listCustomers() {
		return this.customerDAO.listCustomer();
	}

	@Override
	@Transactional
	public Customers getCustomersById(int id) {
		return this.customerDAO.getCustomerById(id);
	}

	@Override
	@Transactional
	public void removeCustomers(int id) {
		this.customerDAO.removeCustomer(id);
	}

}
