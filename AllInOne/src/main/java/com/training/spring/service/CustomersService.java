package com.training.spring.service;

import java.util.List;

import com.training.spring.model.Customers;

public interface CustomersService {

	public void addCustomers(Customers p);
	public void updateCustomers(Customers p);
	public List<Customers> listCustomers();
	public Customers getCustomersById(int id);
	public void removeCustomers(int id);
	
}
