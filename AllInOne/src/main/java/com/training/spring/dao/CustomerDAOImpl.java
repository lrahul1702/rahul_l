package com.training.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.training.spring.model.Customers;
import com.training.spring.model.Person;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addCustomer(Customers c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(c);
		logger.info("Person saved successfully, Person Details="+c);
	}

	@Override
	public void updateCustomer(Customers c) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(c);
		logger.info("Person updated successfully, Person Details="+c);		
	}

	@Override
	public List<Customers> listCustomer() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Customers> customersList = session.createQuery("from Customers").list();
		for(Customers c : customersList){
			logger.info("Customers List::"+c);
		}
		return customersList;
	}

	@Override
	public Customers getCustomerById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Customers c = (Customers) session.load(Customers.class, new Integer(id));
		logger.info("customer loaded successfully, Customer details="+c);
		return c;
	}

	@Override
	public void removeCustomer(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Customers c = (Customers) session.load(Customers.class, new Integer(id));
		if(null != c){
			session.delete(c);
		}
		logger.info("Customers deleted successfully, customers details="+c);
			
	}

}
