package com.training.spring.dao;

import java.util.List;

import com.training.spring.model.Customers;

public interface CustomerDAO {

	public void addCustomer(Customers c);
	public void updateCustomer(Customers c);
	public List<Customers> listCustomer();
	public Customers getCustomerById(int id);
	public void removeCustomer(int id);
}
