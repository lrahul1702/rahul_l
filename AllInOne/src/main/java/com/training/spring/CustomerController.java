package com.training.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.training.spring.model.Customers;
import com.training.spring.model.Person;
import com.training.spring.service.CustomersService;
import com.training.spring.service.PersonService;

@Controller
public class CustomerController {
	
	private CustomersService customersService;
	
	@Autowired(required=true)
	@Qualifier(value="customersService")
	public void setCustomersService(CustomersService cs){
		this.customersService = cs;
	}
	
	@RequestMapping(value = {"/customers","/"})
	public String listCustomers(Model model) {
		model.addAttribute("customers", new Customers());
		model.addAttribute("listCustomers", this.customersService.listCustomers());
		return "customers";
	}
	
	//For add and update customer both
	@RequestMapping(value= "/customers/add", method = RequestMethod.POST)
	public String addCustomers(@ModelAttribute("customers") Customers c){
		System.out.println("#####product :"+c);
			this.customersService.addCustomers(c);
		return "redirect:/customers";
	}

	@RequestMapping(value= "/edit/add/update", method = RequestMethod.POST)
	public String updateCustomers(@ModelAttribute("customers") Customers c){
		System.out.println("#####product updating :"+c);
			this.customersService.updateCustomers(c);
		return "redirect:/customers";
	}
	
	
	@RequestMapping("/remove/{id}")
    public String removeCustomers(@PathVariable("id") int id){
		
        this.customersService.removeCustomers(id);
        return "redirect:/customers";
    }
 
    @RequestMapping("/edit/{id}")
    public String editCustomers(@PathVariable("id") int id, Model model){
        model.addAttribute("customers", this.customersService.getCustomersById(id));
        model.addAttribute("listCustomers", this.customersService.listCustomers());
        return "customers";
    }
	
}
