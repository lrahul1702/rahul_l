package com.training.spring.model;

public class Cinema {

	private int cinema_id;
	private String cinema_name;
	private int cinema_capacity;
	public Cinema()
	{
			super();
	}
	public Cinema(int cinema_id, String cinema_name, int cinema_capacity)
	{
		super();
		this.cinema_id = cinema_id;
		this.cinema_name = cinema_name;
		this.cinema_capacity = cinema_capacity;
	}
	public int getCinema_id() {
		return cinema_id;
	}
	public void setCinema_id(int cinema_id) {
		this.cinema_id = cinema_id;
	}
	public String getCinema_name() {
		return cinema_name;
	}
	public void setCinema_name(String cinema_name) {
		this.cinema_name = cinema_name;
	}
	public int getCinema_capacity() {
		return cinema_capacity;
	}
	public void setCinema_capacity(int cinema_capacity) {
		this.cinema_capacity = cinema_capacity;
	}
	@Override
	public String toString() {
		return "Cinema [cinema_id=" + cinema_id + ", cinema_name=" + cinema_name + ", cinema_capacity="
				+ cinema_capacity + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cinema_capacity;
		result = prime * result + cinema_id;
		result = prime * result + ((cinema_name == null) ? 0 : cinema_name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cinema other = (Cinema) obj;
		if (cinema_capacity != other.cinema_capacity)
			return false;
		if (cinema_id != other.cinema_id)
			return false;
		if (cinema_name == null) {
			if (other.cinema_name != null)
				return false;
		} else if (!cinema_name.equals(other.cinema_name))
			return false;
		return true;
	}
	
	
	
}
