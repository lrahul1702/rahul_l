package com.training.spring.model;

import java.util.Date;

public class Movie_Showings {

	private int movie_showing_id  ;
	private Cinema cinema;
	private Movies movies;
	private Date showing_from_date ;
	private Date showinf_to_date ;
	public Movie_Showings() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Movie_Showings(int movie_showing_id, Cinema cinema, Movies movies, Date showing_from_date,
			Date showinf_to_date) {
		super();
		this.movie_showing_id = movie_showing_id;
		this.cinema = cinema;
		this.movies = movies;
		this.showing_from_date = showing_from_date;
		this.showinf_to_date = showinf_to_date;
	}
	public int getMovie_showing_id() {
		return movie_showing_id;
	}
	public void setMovie_showing_id(int movie_showing_id) {
		this.movie_showing_id = movie_showing_id;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	public Date getShowing_from_date() {
		return showing_from_date;
	}
	public void setShowing_from_date(Date showing_from_date) {
		this.showing_from_date = showing_from_date;
	}
	public Date getShowinf_to_date() {
		return showinf_to_date;
	}
	public void setShowinf_to_date(Date showinf_to_date) {
		this.showinf_to_date = showinf_to_date;
	}
	@Override
	public String toString() {
		return "Movie_Showings [movie_showing_id=" + movie_showing_id + ", cinema=" + cinema + ", movies=" + movies
				+ ", showing_from_date=" + showing_from_date + ", showinf_to_date=" + showinf_to_date + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cinema == null) ? 0 : cinema.hashCode());
		result = prime * result + movie_showing_id;
		result = prime * result + ((movies == null) ? 0 : movies.hashCode());
		result = prime * result + ((showinf_to_date == null) ? 0 : showinf_to_date.hashCode());
		result = prime * result + ((showing_from_date == null) ? 0 : showing_from_date.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie_Showings other = (Movie_Showings) obj;
		if (cinema == null) {
			if (other.cinema != null)
				return false;
		} else if (!cinema.equals(other.cinema))
			return false;
		if (movie_showing_id != other.movie_showing_id)
			return false;
		if (movies == null) {
			if (other.movies != null)
				return false;
		} else if (!movies.equals(other.movies))
			return false;
		if (showinf_to_date == null) {
			if (other.showinf_to_date != null)
				return false;
		} else if (!showinf_to_date.equals(other.showinf_to_date))
			return false;
		if (showing_from_date == null) {
			if (other.showing_from_date != null)
				return false;
		} else if (!showing_from_date.equals(other.showing_from_date))
			return false;
		return true;
	}
	
	
}
