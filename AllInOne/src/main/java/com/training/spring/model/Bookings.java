package com.training.spring.model;

import java.util.Date;

public class Bookings {

	private int bookings_id ;
	private Customers customers;
	private Date booking_for_date;
	private Date booking_made_date;
	private int booking_seat_count;
	public Bookings() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Bookings(int bookings_id, Customers customers, Date booking_for_date, Date booking_made_date,
			int booking_seat_count) {
		super();
		this.bookings_id = bookings_id;
		this.customers = customers;
		this.booking_for_date = booking_for_date;
		this.booking_made_date = booking_made_date;
		this.booking_seat_count = booking_seat_count;
	}
	public int getBookings_id() {
		return bookings_id;
	}
	public void setBookings_id(int bookings_id) {
		this.bookings_id = bookings_id;
	}
	public Customers getCustomers() {
		return customers;
	}
	public void setCustomers(Customers customers) {
		this.customers = customers;
	}
	public Date getBooking_for_date() {
		return booking_for_date;
	}
	public void setBooking_for_date(Date booking_for_date) {
		this.booking_for_date = booking_for_date;
	}
	public Date getBooking_made_date() {
		return booking_made_date;
	}
	public void setBooking_made_date(Date booking_made_date) {
		this.booking_made_date = booking_made_date;
	}
	public int getBooking_seat_count() {
		return booking_seat_count;
	}
	public void setBooking_seat_count(int booking_seat_count) {
		this.booking_seat_count = booking_seat_count;
	}
	@Override
	public String toString() {
		return "Bookings [bookings_id=" + bookings_id + ", customers=" + customers + ", booking_for_date="
				+ booking_for_date + ", booking_made_date=" + booking_made_date + ", booking_seat_count="
				+ booking_seat_count + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((booking_for_date == null) ? 0 : booking_for_date.hashCode());
		result = prime * result + ((booking_made_date == null) ? 0 : booking_made_date.hashCode());
		result = prime * result + booking_seat_count;
		result = prime * result + bookings_id;
		result = prime * result + ((customers == null) ? 0 : customers.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bookings other = (Bookings) obj;
		if (booking_for_date == null) {
			if (other.booking_for_date != null)
				return false;
		} else if (!booking_for_date.equals(other.booking_for_date))
			return false;
		if (booking_made_date == null) {
			if (other.booking_made_date != null)
				return false;
		} else if (!booking_made_date.equals(other.booking_made_date))
			return false;
		if (booking_seat_count != other.booking_seat_count)
			return false;
		if (bookings_id != other.bookings_id)
			return false;
		if (customers == null) {
			if (other.customers != null)
				return false;
		} else if (!customers.equals(other.customers))
			return false;
		return true;
	}
	
}
