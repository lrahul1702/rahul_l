package com.training.spring.model;

public class Ref_Movie_Ratings {

	private int movie_rating ;
	private String movie_rating_description ;
	public Ref_Movie_Ratings() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Ref_Movie_Ratings(int movie_rating, String movie_rating_description) {
		super();
		this.movie_rating = movie_rating;
		this.movie_rating_description = movie_rating_description;
	}
	public int getMovie_rating() {
		return movie_rating;
	}
	public void setMovie_rating(int movie_rating) {
		this.movie_rating = movie_rating;
	}
	public String getMovie_rating_description() {
		return movie_rating_description;
	}
	public void setMovie_rating_description(String movie_rating_description) {
		this.movie_rating_description = movie_rating_description;
	}
	@Override
	public String toString() {
		return "Ref_Movie_Ratings [movie_rating=" + movie_rating + ", movie_rating_description="
				+ movie_rating_description + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + movie_rating;
		result = prime * result + ((movie_rating_description == null) ? 0 : movie_rating_description.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ref_Movie_Ratings other = (Ref_Movie_Ratings) obj;
		if (movie_rating != other.movie_rating)
			return false;
		if (movie_rating_description == null) {
			if (other.movie_rating_description != null)
				return false;
		} else if (!movie_rating_description.equals(other.movie_rating_description))
			return false;
		return true;
	}
	
	
}
