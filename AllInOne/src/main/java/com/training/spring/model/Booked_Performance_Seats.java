package com.training.spring.model;

import java.util.Date;

public class Booked_Performance_Seats {

	private Cinema cinema;
	private Row_Seats row_Seats;
	private int seat_number;
	private Date performance_date;
	private Performance_Numbers performance_Numbers;
	private Bookings bookings;
	public Booked_Performance_Seats() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Booked_Performance_Seats(Cinema cinema, Row_Seats row_Seats, int seat_number, Date performance_date,
			Performance_Numbers performance_Numbers, Bookings bookings) {
		super();
		this.cinema = cinema;
		this.row_Seats = row_Seats;
		this.seat_number = seat_number;
		this.performance_date = performance_date;
		this.performance_Numbers = performance_Numbers;
		this.bookings = bookings;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Row_Seats getRow_Seats() {
		return row_Seats;
	}
	public void setRow_Seats(Row_Seats row_Seats) {
		this.row_Seats = row_Seats;
	}
	public int getSeat_number() {
		return seat_number;
	}
	public void setSeat_number(int seat_number) {
		this.seat_number = seat_number;
	}
	public Date getPerformance_date() {
		return performance_date;
	}
	public void setPerformance_date(Date performance_date) {
		this.performance_date = performance_date;
	}
	public Performance_Numbers getPerformance_Numbers() {
		return performance_Numbers;
	}
	public void setPerformance_Numbers(Performance_Numbers performance_Numbers) {
		this.performance_Numbers = performance_Numbers;
	}
	public Bookings getBookings() {
		return bookings;
	}
	public void setBookings(Bookings bookings) {
		this.bookings = bookings;
	}
	@Override
	public String toString() {
		return "Booked_Performance_Seats [cinema=" + cinema + ", row_Seats=" + row_Seats + ", seat_number="
				+ seat_number + ", performance_date=" + performance_date + ", performance_Numbers="
				+ performance_Numbers + ", bookings=" + bookings + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookings == null) ? 0 : bookings.hashCode());
		result = prime * result + ((cinema == null) ? 0 : cinema.hashCode());
		result = prime * result + ((performance_Numbers == null) ? 0 : performance_Numbers.hashCode());
		result = prime * result + ((performance_date == null) ? 0 : performance_date.hashCode());
		result = prime * result + ((row_Seats == null) ? 0 : row_Seats.hashCode());
		result = prime * result + seat_number;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booked_Performance_Seats other = (Booked_Performance_Seats) obj;
		if (bookings == null) {
			if (other.bookings != null)
				return false;
		} else if (!bookings.equals(other.bookings))
			return false;
		if (cinema == null) {
			if (other.cinema != null)
				return false;
		} else if (!cinema.equals(other.cinema))
			return false;
		if (performance_Numbers == null) {
			if (other.performance_Numbers != null)
				return false;
		} else if (!performance_Numbers.equals(other.performance_Numbers))
			return false;
		if (performance_date == null) {
			if (other.performance_date != null)
				return false;
		} else if (!performance_date.equals(other.performance_date))
			return false;
		if (row_Seats == null) {
			if (other.row_Seats != null)
				return false;
		} else if (!row_Seats.equals(other.row_Seats))
			return false;
		if (seat_number != other.seat_number)
			return false;
		return true;
	}
	
}
