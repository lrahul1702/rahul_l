package com.training.spring.model;

public class Row_Seats {

	private Cinema cinema;
	private int row_number;
	private int seat_count ;
	private int price;
	public Row_Seats() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Row_Seats(Cinema cinema, int row_number, int seat_count, int price) {
		super();
		this.cinema = cinema;
		this.row_number = row_number;
		this.seat_count = seat_count;
		this.price = price;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public int getRow_number() {
		return row_number;
	}
	public void setRow_number(int row_number) {
		this.row_number = row_number;
	}
	public int getSeat_count() {
		return seat_count;
	}
	public void setSeat_count(int seat_count) {
		this.seat_count = seat_count;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Row_Seats [cinema=" + cinema + ", row_number=" + row_number + ", seat_count=" + seat_count + ", price="
				+ price + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cinema == null) ? 0 : cinema.hashCode());
		result = prime * result + price;
		result = prime * result + row_number;
		result = prime * result + seat_count;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Row_Seats other = (Row_Seats) obj;
		if (cinema == null) {
			if (other.cinema != null)
				return false;
		} else if (!cinema.equals(other.cinema))
			return false;
		if (price != other.price)
			return false;
		if (row_number != other.row_number)
			return false;
		if (seat_count != other.seat_count)
			return false;
		return true;
	}
	
	
}
