package com.training.spring.model;

public class Ref_Seat_Status {

	private int seat_status_code;
	private String seat_status_description ;
	public Ref_Seat_Status() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Ref_Seat_Status(int seat_status_code, String seat_status_description) {
		super();
		this.seat_status_code = seat_status_code;
		this.seat_status_description = seat_status_description;
	}
	public int getSeat_status_code() {
		return seat_status_code;
	}
	public void setSeat_status_code(int seat_status_code) {
		this.seat_status_code = seat_status_code;
	}
	public String getSeat_status_description() {
		return seat_status_description;
	}
	public void setSeat_status_description(String seat_status_description) {
		this.seat_status_description = seat_status_description;
	}
	@Override
	public String toString() {
		return "Ref_Seat_Status [seat_status_code=" + seat_status_code + ", seat_status_description="
				+ seat_status_description + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + seat_status_code;
		result = prime * result + ((seat_status_description == null) ? 0 : seat_status_description.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ref_Seat_Status other = (Ref_Seat_Status) obj;
		if (seat_status_code != other.seat_status_code)
			return false;
		if (seat_status_description == null) {
			if (other.seat_status_description != null)
				return false;
		} else if (!seat_status_description.equals(other.seat_status_description))
			return false;
		return true;
	}
	
	
}
