package com.training.spring.model;

public class Performance_Numbers {

	private int performance_Numbers ;
	private String performance_start_time ;
	private String performance_end_time ;
	public Performance_Numbers() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Performance_Numbers(int performance_Numbers, String performance_start_time, String performance_end_time) {
		super();
		this.performance_Numbers = performance_Numbers;
		this.performance_start_time = performance_start_time;
		this.performance_end_time = performance_end_time;
	}
	public int getPerformance_Numbers() {
		return performance_Numbers;
	}
	public void setPerformance_Numbers(int performance_Numbers) {
		this.performance_Numbers = performance_Numbers;
	}
	public String getPerformance_start_time() {
		return performance_start_time;
	}
	public void setPerformance_start_time(String performance_start_time) {
		this.performance_start_time = performance_start_time;
	}
	public String getPerformance_end_time() {
		return performance_end_time;
	}
	public void setPerformance_end_time(String performance_end_time) {
		this.performance_end_time = performance_end_time;
	}
	@Override
	public String toString() {
		return "Performance_Numbers [performance_Numbers=" + performance_Numbers + ", performance_start_time="
				+ performance_start_time + ", performance_end_time=" + performance_end_time + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + performance_Numbers;
		result = prime * result + ((performance_end_time == null) ? 0 : performance_end_time.hashCode());
		result = prime * result + ((performance_start_time == null) ? 0 : performance_start_time.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Performance_Numbers other = (Performance_Numbers) obj;
		if (performance_Numbers != other.performance_Numbers)
			return false;
		if (performance_end_time == null) {
			if (other.performance_end_time != null)
				return false;
		} else if (!performance_end_time.equals(other.performance_end_time))
			return false;
		if (performance_start_time == null) {
			if (other.performance_start_time != null)
				return false;
		} else if (!performance_start_time.equals(other.performance_start_time))
			return false;
		return true;
	}
	
}
