package com.training.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMERS")
public class Customers {

	@Id
	@Column(name="id")
	private int customers_id ;
	
	private String customers_name;
	
	private long customers_phone_number;
	
	private Date customers_dob ;
	
	private String customers_email_id;
	
	private String customers_password;
	
	public Customers() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Customers(int customers_id, String customers_name, long customers_phone_number, Date customers_dob,
			String customers_email_id, String customers_password) {
		super();
		this.customers_id = customers_id;
		this.customers_name = customers_name;
		this.customers_phone_number = customers_phone_number;
		this.customers_dob = customers_dob;
		this.customers_email_id = customers_email_id;
		this.customers_password = customers_password;
	}
	public int getCustomers_id() {
		return customers_id;
	}
	public void setCustomers_id(int customers_id) {
		this.customers_id = customers_id;
	}
	public String getCustomers_name() {
		return customers_name;
	}
	public void setCustomers_name(String customers_name) {
		this.customers_name = customers_name;
	}
	public long getCustomers_phone_number() {
		return customers_phone_number;
	}
	public void setCustomers_phone_number(long customers_phone_number) {
		this.customers_phone_number = customers_phone_number;
	}
	public Date getCustomers_dob() {
		return customers_dob;
	}
	public void setCustomers_dob(Date customers_dob) {
		this.customers_dob = customers_dob;
	}
	public String getCustomers_email_id() {
		return customers_email_id;
	}
	public void setCustomers_email_id(String customers_email_id) {
		this.customers_email_id = customers_email_id;
	}
	public String getCustomers_password() {
		return customers_password;
	}
	public void setCustomers_password(String customers_password) {
		this.customers_password = customers_password;
	}
	@Override
	public String toString() {
		return "Customers [customers_id=" + customers_id + ", customers_name=" + customers_name
				+ ", customers_phone_number=" + customers_phone_number + ", customers_dob=" + customers_dob
				+ ", customers_email_id=" + customers_email_id + ", customers_password=" + customers_password + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customers_dob == null) ? 0 : customers_dob.hashCode());
		result = prime * result + ((customers_email_id == null) ? 0 : customers_email_id.hashCode());
		result = prime * result + customers_id;
		result = prime * result + ((customers_name == null) ? 0 : customers_name.hashCode());
		result = prime * result + ((customers_password == null) ? 0 : customers_password.hashCode());
		result = prime * result + (int) (customers_phone_number ^ (customers_phone_number >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customers other = (Customers) obj;
		if (customers_dob == null) {
			if (other.customers_dob != null)
				return false;
		} else if (!customers_dob.equals(other.customers_dob))
			return false;
		if (customers_email_id == null) {
			if (other.customers_email_id != null)
				return false;
		} else if (!customers_email_id.equals(other.customers_email_id))
			return false;
		if (customers_id != other.customers_id)
			return false;
		if (customers_name == null) {
			if (other.customers_name != null)
				return false;
		} else if (!customers_name.equals(other.customers_name))
			return false;
		if (customers_password == null) {
			if (other.customers_password != null)
				return false;
		} else if (!customers_password.equals(other.customers_password))
			return false;
		if (customers_phone_number != other.customers_phone_number)
			return false;
		return true;
	}
	
}
