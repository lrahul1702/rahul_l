package com.training.spring.model;

public class Movies {

	private int movie_id ;
	private String movie_name ;
	private String movie_language ;
	private String movie_genre ;
	private Ref_Movie_Ratings ref_Movie_Ratings;
	public Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Movies(int movie_id, String movie_name, String movie_language, String movie_genre,
			Ref_Movie_Ratings ref_Movie_Ratings) {
		super();
		this.movie_id = movie_id;
		this.movie_name = movie_name;
		this.movie_language = movie_language;
		this.movie_genre = movie_genre;
		this.ref_Movie_Ratings = ref_Movie_Ratings;
	}
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	public String getMovie_name() {
		return movie_name;
	}
	public void setMovie_name(String movie_name) {
		this.movie_name = movie_name;
	}
	public String getMovie_language() {
		return movie_language;
	}
	public void setMovie_language(String movie_language) {
		this.movie_language = movie_language;
	}
	public String getMovie_genre() {
		return movie_genre;
	}
	public void setMovie_genre(String movie_genre) {
		this.movie_genre = movie_genre;
	}
	public Ref_Movie_Ratings getRef_Movie_Ratings() {
		return ref_Movie_Ratings;
	}
	public void setRef_Movie_Ratings(Ref_Movie_Ratings ref_Movie_Ratings) {
		this.ref_Movie_Ratings = ref_Movie_Ratings;
	}
	@Override
	public String toString() {
		return "Movies [movie_id=" + movie_id + ", movie_name=" + movie_name + ", movie_language=" + movie_language
				+ ", movie_genre=" + movie_genre + ", ref_Movie_Ratings=" + ref_Movie_Ratings + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((movie_genre == null) ? 0 : movie_genre.hashCode());
		result = prime * result + movie_id;
		result = prime * result + ((movie_language == null) ? 0 : movie_language.hashCode());
		result = prime * result + ((movie_name == null) ? 0 : movie_name.hashCode());
		result = prime * result + ((ref_Movie_Ratings == null) ? 0 : ref_Movie_Ratings.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movies other = (Movies) obj;
		if (movie_genre == null) {
			if (other.movie_genre != null)
				return false;
		} else if (!movie_genre.equals(other.movie_genre))
			return false;
		if (movie_id != other.movie_id)
			return false;
		if (movie_language == null) {
			if (other.movie_language != null)
				return false;
		} else if (!movie_language.equals(other.movie_language))
			return false;
		if (movie_name == null) {
			if (other.movie_name != null)
				return false;
		} else if (!movie_name.equals(other.movie_name))
			return false;
		if (ref_Movie_Ratings == null) {
			if (other.ref_Movie_Ratings != null)
				return false;
		} else if (!ref_Movie_Ratings.equals(other.ref_Movie_Ratings))
			return false;
		return true;
	}
	
	
	
}
