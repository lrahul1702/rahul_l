import java.text.DecimalFormat;
public class Addition_float{

	public static void main(String[] arg){

		if(arg.length != 2){
		System.out.println("no. of args missmatch. Enter only 2 numbers");
		return;


}

	DecimalFormat df = new DecimalFormat("#,###,##0.00");
	float a = Float.parseFloat(arg[0]);
	float b = Float.parseFloat(arg[1]);
	float c = a + b;
	System.out.println(" a = "+ df.format(a) + " b =" + df.format(b) + 
								" sum =" + df.format(c)); 
}

}